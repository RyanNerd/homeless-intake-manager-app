<?php
declare(strict_types=1);

namespace pantry\User;

use pantry\Slim\App;
use pantry\Slim\Authenticate;
use function valid_num_args;

class UserController
{
    public function register(App $slim)
    {
        assert(valid_num_args());

        // Query params: household_id=#
        $slim->get('/users[/{id}]', UserGetAction::class)
            ->add(Authenticate::class);
        $slim->post('/users/authenticate', UserAuthenticateAction::class);
        $slim->patch('/users', UserPatchAction::class);
        $slim->post('/users', UserPostAction::class);
        $slim->post('/users/password-reset', UserPasswordResetAction::class);
    }
}