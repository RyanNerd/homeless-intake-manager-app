<?php
declare(strict_types=1);

namespace pantry\User;

use pantry\Slim\ActionBase;
use pantry\Slim\ResponseBody;
use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use Slim\Route;
use function valid_num_args;
use pantry\Models\User;

class UserGetAction extends ActionBase
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $attributes = $request->getAttributes();

        /** @var ResponseBody $responseBody */
        $responseBody = $attributes['response_body'];

        /** @var Route $route */
        $route = $attributes['route'];
        $id = $route->getArgument('id');
        if (isset($id) && strlen($id) !== 0) {
            $id = (int)$id;
            if ($id > 0) {
                $users = User::find($id);
                $users = isset($users) ? [$users->toArray()] : null;
                unset($users['PasswordHash']);
                unset($users['AuthKey']);
            } else {
                $responseBody = $responseBody->
                withStatus(400)->
                withMessage('invalid Id');
                return $response->withJson($responseBody())->withStatus(400);
            }
        } else {
            $filter = $request->getQueryParams();
            $users = $this->buildSelect(new User(), $filter);

            if (count($users) === 0) {
                $users = null;
            } else {
                $users = $users->toArray();
                foreach ($users as &$user) {
                    unset($user['PasswordHash']);
                    unset($user['AuthKey']);
                }
            }
        }

        $responseBody = $responseBody->withData($users)->withStatus(($users === null) ? 404 : 200);
        return $response->withJson($responseBody())->withStatus($responseBody->getStatus());
    }
}
