<?php
declare(strict_types=1);

namespace pantry\User;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\User;

/**
 * Class UserAuthenticateAction
 */
class UserAuthenticateAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        // Get the body as an associative array
        $body = $request->getParsedBody();
        $email = $body['Email'];
        $password = $body['Password'];

        // Assume not authorized as the response.
        $data =
        [
            'success' => false,
            'status' => 401,
            'data' => null
        ];

        $user = User::where('Email', '=', $email)->first();
        // $email can also be a user name so if looking up by email fails try by user name.
        if ($user === null) {
            $user = User::where('UserName', '=', $email)->first();
        }

        // Did we find a User record AND is the user Active?
        if ($user !== null && $user->Active) {
            // Does the password check out?
            // TODO: Remove true from password check below. IMPORTANT!
            if (true || password_verify($password, $user->PasswordHash)) {
                $record =
                [
                    'Id' => $user->Id,
                    'AuthKey' => $user->AuthKey,
                    'IsAdmin' => $user->IsAdmin,
                    'FirstName' => $user->FirstName,
                    'LastName' => $user->LastName,
                    'UserName' => $user->UserName,
                    'Email' => $user->Email,
                    'MustResetPassword' => $user->MustResetPassword
                ];

                $data =
                [
                    'success' => true,
                    'status' => 200,
                    'data' => $record
                ];
            }
        } else {
            $data =
            [
                'success' => false,
                'status' => 404,
                'data' => null
            ];
        }

        return $response->withJson($data)->withStatus($data['status']);
    }
}
