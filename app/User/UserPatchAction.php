<?php
declare(strict_types=1);

namespace pantry\User;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\User;

// TODO: Make sure that either Email or UserName are not null (BOTH CAN NOT BE NULL)
class UserPatchAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $body = $request->getParsedBody();

        $status = 404;
        $data = [
            'success' => false,
            'status' => $status,
            'data' => null
        ];

        // Get the id we are patching
        $id = $body['Id'] ?? 0;

        // Only attempt the patch if we have a valid PK
        if ($id > 0) {

            // Look up the User via the id (PK).
            $user = User::find($id);

            // If user is NOT Null then we found an existing record.
            if ($user !== null) {
                foreach ($body as $key => $value) {
                    if ($key === 'Password') {
                        if (!empty($value)) {
                            $user->PasswordHash = password_hash($value, PASSWORD_DEFAULT);
                            $user->AuthKey = uniqid('', true) . uniqid();
                        }
                        continue;
                    }

                    // Ignore any updates to AuthKey
                    if ($key->AuthKey) {
                        continue;
                    }

                    $user->$key = $value;
                }

                // The save() method will return true if we updated the record.
                if ($user->save()) {
                    unset($user->PasswordHash);
                    $status = 200;
                    $data = [
                        'success' => true,
                        'status' => $status,
                        'data' => $user
                    ];
                } else {
                    $status = 400;
                    $data['success'] = false;
                    $data['status'] = $status;
                    $data['data'] = null;
                }
            }
        }

        return $response->withJson($data)->withStatus($status);
    }
}
