<?php
declare(strict_types=1);

namespace pantry\User;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\User;

/**
 * Class UserPostAction
 */
class UserPostAction
{
    // TODO: Make sure that either Email or UserName are not null (BOTH CAN NOT BE NULL)
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        // Assume failure
        $data =
        [   'success' => false,
            'status' => 500,
            'data' => null
        ];

        // Get the body as an associative array
        $body = $request->getParsedBody();

        // Add new User record
        $user = new User();

        // Replace each key value from the JSON body into the User model and save.
        foreach ($body as $key => $value) {
            if ($key === 'Password') {
                $user->PasswordHash = password_hash($value, PASSWORD_DEFAULT);
                continue;
            }

            // Ignore AuthKey as we generate these server side.
            if ($key === 'AuthKey') {
                continue;
            }

            $user->$key = $value;
        }

        $user->AuthKey = uniqid('', true) . uniqid();

        if ($user->save()) {
            unset($user->PasswordHash);
            unset($user->AuthKey);

            $data = [
                'success' => true,
                'status' => 200,
                'data' => $user
            ];
        } else {
            $status = 400;
            $data['success'] = false;
            $data['status'] = $status;
            $data['data'] = null;
        }

        return $response->withJson($data)->withStatus($data['status']);
    }
}
