<?php
declare(strict_types=1);

namespace pantry\Poverty;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\Poverty;

class PovertyPatchAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $body = $request->getParsedBody();

        $status = 404;
        $data = [
            'success' => false,
            'status' => $status,
            'data' => null
        ];

        // Get the id we are patching
        $id = $body['Id'] ?? 0;

        // Only attempt the patch if we have a valid PK
        if ($id > 0) {

            // Look up the Intake via the id (PK).
            $poverty = Poverty::find($id);

            // If intake is NOT Null then we found an existing record.
            if ($poverty !== null) {
                foreach ($body as $key => $value) {
                    if ($key === 'Id') {
                        continue;
                    }

                    $poverty->$key = $value;
                }

                // The save() method will return true if we updated the record.
                if ($poverty->save()) {
                    $status = 200;
                    $data = [
                        'success' => true,
                        'status' => $status,
                        'data' => $poverty
                    ];
                } else {
                    $status = 400;
                    $data['success'] = false;
                    $data['status'] = $status;
                    $data['data'] = null;
                }
            }
        }

        return $response->withJson($data)->withStatus($status);
    }
}
