<?php
declare(strict_types=1);

namespace pantry\Poverty;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\Poverty;

class PovertyGetAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $route = $request->getAttribute('route');
        $id = $route->getArgument('id') ?? 0;

        if ($id > 0) {
            $records = Poverty::find($id);
            $records = isset($records) ? [$records->toArray()] : null;
        } else {
            $poverty = Poverty::all();
            $records = array_values($poverty->toArray());
        }

        $status = ($records !== null) ? 200 : 404;

        $data = [
            'success' => ($status === 200),
            'status' => $status,
            'data' => $records
        ];

        return $response->withJson($data)->withStatus($data['status']);
    }
}
