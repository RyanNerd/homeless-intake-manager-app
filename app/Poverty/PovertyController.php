<?php
declare(strict_types=1);

namespace pantry\Poverty;

use pantry\Slim\App;
use pantry\Slim\Authenticate;
use function valid_num_args;

class PovertyController
{
    public function register(App $slim)
    {
        assert(valid_num_args());

        $slim->get('/poverty[/{id}]', PovertyGetAction::class)
            ->add(Authenticate::class);
        $slim->patch('/poverty', PovertyPatchAction::class)
            ->add(Authenticate::class);
    }
}