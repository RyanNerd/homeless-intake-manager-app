<?php
declare(strict_types=1);

namespace pantry\Intake;

use pantry\Slim\ActionBase;
use pantry\Slim\ResponseBody;
use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use Slim\Route;
use function valid_num_args;
use pantry\Models\Intake;

class IntakeGetAction extends ActionBase
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $attributes = $request->getAttributes();

        /** @var ResponseBody $responseBody */
        $responseBody = $attributes['response_body'];

        /** @var Route $route */
        $route = $attributes['route'];
        $id = $route->getArgument('id');
        if (isset($id) && strlen($id) !== 0) {
            $id = (int)$id;
            if ($id > 0) {
                $intakes = Intake::find($id);
                $intakes = isset($intakes) ? [$intakes->toArray()] : null;
            } else {
                $responseBody = $responseBody->
                withStatus(400)->
                withMessage('invalid Id');
                return $response->withJson($responseBody())->withStatus(400);
            }
        } else {
            $filter = $request->getQueryParams();
            $intakes = $this->buildSelect(new Intake(), $filter);

            if (count($intakes) === 0) {
                $intakes = null;
            } else {
                $intakes = $intakes->toArray();
            }
        }

        $responseBody = $responseBody->withData($intakes)->withStatus(($intakes === null) ? 404 : 200);
        return $response->withJson($responseBody())->withStatus($responseBody->getStatus());
    }

}
