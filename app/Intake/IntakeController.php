<?php
declare(strict_types=1);

namespace pantry\Intake;

use pantry\Slim\App;
use pantry\Slim\Authenticate;
use function valid_num_args;

class IntakeController
{
    public function register(App $slim)
    {
        assert(valid_num_args());

        // Query params: household_id=#
        $slim->get('/intakes[/{id}]', IntakeGetAction::class)
            ->add(Authenticate::class);
        $slim->post('/intakes', IntakePostAction::class)
            ->add(Authenticate::class);
        $slim->patch('/intakes', IntakePatchAction::class)
            ->add(Authenticate::class);
    }
}
