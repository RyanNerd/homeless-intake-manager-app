<?php
declare(strict_types=1);

namespace pantry\Storage;

use pantry\Slim\App;
use pantry\Slim\Authenticate;
use function valid_num_args;

class StorageController
{
    public function register(App $slim)
    {
        assert(valid_num_args());

        $slim->get('/storage/{id}', StorageGetAction::class)
            ->add(Authenticate::class);
        $slim->post('/storage', StoragePostAction::class)
            ->add(Authenticate::class);
        $slim->patch('/storage', StoragePatchAction::class)
            ->add(Authenticate::class);
    }
}