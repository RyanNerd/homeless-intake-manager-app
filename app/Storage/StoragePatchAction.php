<?php
declare(strict_types=1);

namespace pantry\Storage;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\Storage;

// TODO: Correct status codes for failure to write, etc.
class StoragePatchAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $body = $request->getParsedBody();

        $status = 404;
        $data = [
            'success' => false,
            'status' => $status,
            'data' => null
        ];

        // Get the id we are patching
        $id = $body['Id'] ?? 0;

        // Only attempt the patch if we have a valid PK
        if ($id > 0) {

            // Look up the Storage via the id (PK).
            $storage = Storage::find($id);

            // If storage is NOT Null then we found an existing record.
            if ($storage !== null) {
                // If there is already an existing file it will be replaced.
                $fileName = $storage->FileName;
                $filePath = realpath(getenv('IMAGE_DIR'));
                $filePath = $filePath . DIRECTORY_SEPARATOR . $fileName;
                $fileSaved = file_put_contents($filePath, $body['Content']);
                if ($fileSaved !== false && $fileSaved > 0) {
                    $storage->FileName = $fileName;
                    $storage->MimeType = $body['MimeType'] ?? null;

                    if ($storage->save()) {
                        $status = 200;
                        $data =
                        [
                            'success' => true,
                            'status' => 200,
                            'data' =>
                            [
                                'Id' => $storage->Id,
                                'FileName' => $fileName,
                                'MimeType' => $storage->MimeType
                            ]
                        ];
                    }
                }
            }
        }

        return $response->withJson($data)->withStatus($status);
    }
}
