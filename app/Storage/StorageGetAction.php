<?php
declare(strict_types=1);

namespace pantry\Storage;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\Storage;

class StorageGetAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $route = $request->getAttribute('route');
        $id = $route->getArgument('id') ?? 0;

        if ($id > 0) {
            $records = Storage::find($id);
            if (isset($records)) {
                $records->toArray();
                $filePath = getenv('IMAGE_DIR') . '/' . $records['FileName'];
                $records['Content'] = file_get_contents($filePath);

                // TODO: Error control
            }
        } else {
            $records = null;
        }

        $status = ($records !== null) ? 200 : 404;

        $data = [
            'success' => ($status === 200),
            'status' => $status,
            'data' => $records
        ];

        return $response->withJson($data)->withStatus($data['status']);
    }
}
