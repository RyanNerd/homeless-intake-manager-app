<?php
declare(strict_types=1);

namespace pantry\Storage;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\Storage;

class StoragePostAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        // Assume failure
        $data =
        [
            'success' => false,
            'status' => 500,
            'data' => null
        ];

        // Get the body as an associative array
        $body = $request->getParsedBody();

        // Add new Storage record
        $storage = new Storage();

        $fileName = uniqid();
        $filePath = realpath(getenv('IMAGE_DIR'));
        $filePath = $filePath . DIRECTORY_SEPARATOR . $fileName;
        $fileSaved = file_put_contents($filePath, $body['Content']);
        if ($fileSaved !== false && $fileSaved > 0) {
            $storage->FileName = $fileName;
            $storage->MimeType = $body['MimeType'] ?? null;
            if ($storage->save()) {
                $data = [
                    'success' => true,
                    'status' => 200,
                    'data' =>
                    [
                        'Id' => $storage->Id,
                        'FileName' => $fileName,
                        'MimeType' => $storage->MimeType
                    ]
                ];
            }
        }

        return $response->withJson($data)->withStatus($data['status']);
    }
}
