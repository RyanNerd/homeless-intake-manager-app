<?php
declare(strict_types=1);

namespace pantry\Slim;

use DI\ContainerBuilder;
use DI\Bridge\Slim\App as BridgeApp;
use Illuminate\Database\Capsule\Manager as Capsule;
use pantry\County\CountyController;
use pantry\Poverty\PovertyController;
use pantry\Reports\ServiceController;
use pantry\Storage\StorageController;
use pantry\User\UserController;
use function valid_num_args;

use pantry\Household\HouseholdController;
use pantry\Intake\IntakeController;
use pantry\Member\MemberController;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;

// TODO: All Entities that have UserId need to be updated from the ResponseBody attribute.
class App extends BridgeApp
{
    /**
     * @var Capsule|null
     */
    protected $capsule = null;

    public function __construct()
    {
        assert(valid_num_args());

        parent::__construct();

        $container = $this->getContainer();

        if ($this->capsule === null) {
            $this->capsule = $container->get(Capsule::class);
        }


        $v1 = $this->group('/v1', function () use ($container)
        {
            $container->get(HouseholdController::class)->register($this);
            $container->get(MemberController::class)->register($this);
            $container->get(IntakeController::class)->register($this);
            $container->get(PovertyController::class)->register($this);
            $container->get(UserController::class)->register($this);
            $container->get(StorageController::class)->register($this);
            $container->get(CountyController::class)->register($this);
            $container->get(ServiceController::class)->register($this);
        });

        // Accept all routes for options
        $this->options('/{routes:.+}', function (Request $request, Response $response, $args) {
            return $response;
        });

        // Override the CORS stupidity and Slim's exception handlers via Middleware LIFE
        $this->add(function (Request $request, Response $response, callable $next): ResponseInterface {
            try {
                /** @var Response $response */
                $response = $next($request, $response);

                return $response
                    ->withHeader('Access-Control-Allow-Origin', '*')
                    ->withHeader('Access-Control-Allow-Credentials', true)
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, OPTIONS')
                    ->withHeader('Content-Type', 'application/json');
            } catch (\Throwable $throwable) {
                $handleErrors = new HandleRuntimeError();
                return $handleErrors($request, $response, $throwable);
            }
        });
    }


    /**
     * DI-Bridge configuration hook.
     */
    protected function configureContainer(ContainerBuilder $builder): void
    {
        assert(valid_num_args());

        include_once(__DIR__ . '/../../config/_env.php');
        foreach (glob(__DIR__ . '/../../config/*.php') as $definitions) {
            if (strpos($definitions, '_env.php') === false) {
                $builder->addDefinitions(realpath($definitions));
            }
        }
    }
}
