<?php
declare(strict_types=1);

namespace pantry\Slim;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Stringy\StaticStringy as S;
use function valid_num_args;

/**
 * Class ActionBase
 */
abstract class ActionBase
{
    /**
     * @param Model $model
     * @param array $queryParams
     * @return Collection
     */
    protected function buildSelect(Model $model, array $queryParams): Collection
    {
        assert(valid_num_args());

        // Parse query parameters
        $isSoftSearch = (bool)($queryParams['soft_search'] ?? 0);
        $orderBy = $queryParams['order_by'] ?? 'Id';
        $orderDirection = $queryParams['order_direction'] ?? 'desc';
        $fieldValue = '';
        $columns = [];

        // TODO: order_by
        // TODO: order_direction
        // TODO: Multiple search columns
        // TODO: complex_sql?
        // TODO: page_size?
        // TODO: count?
        foreach ($queryParams as $key => $value) {
            // Non-soft search columns have a double underscore.
            if (substr($key, 0, 2) === "__") {
                break;
            }

            // Search columns have an underscore as their first character.
            if (substr($key,0,1) === '_') {
                $columnName = S::upperCamelize(substr($key,1));
                $fieldValue = $value;
                break;
            }
        }

        $fieldValue .= ($isSoftSearch === true) ? '%' : '';
        if (!empty($columnName))
        {
            return $model->
                where($columnName, ($isSoftSearch === true) ? 'LIKE' : '=', $fieldValue)->
                orderBy($orderBy, $orderDirection)->
                get();
        }

        return $model::all();
    }
}