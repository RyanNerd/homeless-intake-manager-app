<?php
declare(strict_types=1);

namespace pantry\Slim;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use function valid_num_args;
use pantry\Models\User;

class Authenticate
{
    /**
     * @var ResponseBody
     */
    protected $responseBody;

    /**
     * Cache for keys that have been authenticated.
     *
     * @var array
     */
    static protected $authKeys = [];

    public function __construct(ResponseBody $responseBody)
    {
        assert(valid_num_args());

        $this->responseBody = $responseBody;
    }

    public function __invoke(Request $request, Response $response, callable $next) : ResponseInterface
    {
        // Get the authorization key
        $authKey = $request->getQueryParam('auth_key') ?? '';

        // Does the key exist?
        if ($authKey !== '') {
            // Is the key not already in the cache?
            if (!isset(self::$authKeys[$authKey]) || !is_int(self::$authKeys[$authKey])) {
                // Find the user record that matches the key.
                $user = User::where('AuthKey', '=', $authKey)->first();
                // Did we find a user that matches the record?
                if ($user !== null) {
                    // Add the key to the cache.
                    self::$authKeys[$authKey] = $user->Id;
                }
            }
        }

        // If the given authKey is in the cache then we are good to go.
        if (isset(self::$authKeys[$authKey])) {
            return $next($request->withAttribute('response_body', $this->responseBody->setIsAuthorized()->setUserId(self::$authKeys[$authKey])), $response);
        }

        // Not an authorized request -- short circuit further processing and return an unauthorized status.
        $responseBody = $this->responseBody->withStatus(401);
        return $response->withJson($responseBody())->withStatus($responseBody->getStatus());
    }
}