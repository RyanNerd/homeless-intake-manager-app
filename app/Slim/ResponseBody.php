<?php
declare(strict_types=1);

namespace pantry\Slim;

use function valid_num_args;

class ResponseBody
{
    /** @var array | null */
    protected $data = null;

    /** @var int */
    protected $status = 200;

    /** @var bool */
    protected $authorized = false;

    /**
     * User Id that authenticated.
     *
     * @var int | null
     */
    protected $userId = null;

    /** @var string */
    protected $message = '';

    /** @var array */
    protected $missing = [];

    /**
     * Generate the response array
     */
    public function __invoke(): array
    {
        assert(valid_num_args());

        return
        [
            'authorized' => $this->authorized,
            'success' => ($this->status === 200),
            'status' => $this->status,
            'data' => $this->data,
            'missing' => $this->missing,
            'message' => $this->message,
            'timestamp' => time()
        ];
    }

    public function hasMissingRequiredOrInvalid(): bool
    {
        return (isset($this->missing['invalid']) || isset($this->missing['required']));
    }

    /**
     * Sets that the request as is authorized.
     *
     * @return self
     */
    public function setIsAuthorized(): self
    {
        assert(valid_num_args());

        $clone = clone $this;
        $clone->authorized = true;
        return $clone;
    }

    /**
     * Sets the authorized user id.
     *
     * @param int $userId
     * @return self
     */
    public function setUserId(int $userId): self
    {
        assert(valid_num_args());
        assert($userId > 0);

        $clone = clone $this;
        $clone->userId = $userId;
        return $clone;
    }

    public function getUserId(): ?int
    {
        assert(valid_num_args());

        return $this->userId;
    }

    /**
     * Sets the optional request options.
     *
     * @param array $optional
     * @return self
     */
    public function setOptional(array $optional): self
    {
        assert(valid_num_args());
        assert(count($optional) !== 0);

        $clone = clone $this;
        $clone->optional = $optional;
        return $clone;
    }

    /**
     * Sets the required request options.
     *
     * @param array $required
     * @return self
     */
    public function setRequired(array $required): self
    {
        assert(valid_num_args());
        assert(count($required) !== 0);

        $clone = clone $this;
        $clone->required = $required;
        return $clone;
    }

    /**
     * Register a parameter as optional, required or invalid.
     *
     * @param string $section
     * @param string $name
     * @param string $type
     */
    public function registerParam(string $section, string $name, string $type): void
    {
        assert(valid_num_args());
        assert(in_array($section, ['optional', 'required', 'invalid']));
        assert(strlen($name) !== 0);
        assert(strlen($type) !== 0);

        $data = $this->missing[$section] ?? [];
        $data[$name] = $data[$name] ?? $type;
        $this->missing[$section] = $data;
    }


    /**
     * Sets the response body payload.
     *
     * @param array $data
     * @return self
     */
    public function withData(?array $data): self
    {
        assert(valid_num_args());

        $clone = clone $this;
        $clone->data = $data;
        return $clone;
    }

    /**
     * Sets the response status code.
     *
     * @param int $status
     * @return self
     */
    public function withStatus(int $status): self
    {
        assert(valid_num_args());
        assert($status > 99 && $status < 1000);

        $clone = clone $this;
        $clone->status = $status;
        return $clone;
    }

    /**
     * Return the http status code
     *
     * @return int
     */
    public function getStatus(): int
    {
        assert(valid_num_args());

        return $this->status;
    }

    public function withMessage(string $message): self
    {
        assert(valid_num_args());
        assert(strlen($message) !==0);

        $clone = clone $this;
        $clone->message = $message;
        return $clone;
    }
}