<?php
declare(strict_types=1);

namespace pantry\Reports;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use function valid_num_args;

class ServiceReport
{
    /**
     * @var Capsule
     */
    protected $dbManager;

    public function __construct(Capsule $manager)
    {
        assert(valid_num_args());

        $this->dbManager = $manager;
    }

    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $queryParameters = $request->getParams();
        $beginDate = $queryParameters['begin_date'] ?? '';
        $endDate = $queryParameters['end_date'] ?? '';

        if ($beginDate === '' || $endDate === '') {
            $data = [
                'success' => false,
                'status' => 400,
                'data' => null
            ];

            return $response->withJson($data)->withStatus($data['status']);
        }

        $householdServed = null;

        $pdo = $this->dbManager::connection()->getPdo();
        $pdoStatement = $pdo->query('SELECT COUNT(*) as HouseholdsServed FROM Household');
        $records = $pdoStatement->fetchAll();
        $status = ($records !== null) ? 200 : 404;

        if ($status === 200) {
            $householdServed = $records[0]['HouseholdsServed'];
        }

        $data = [
            'success' => ($status === 200),
            'status' => $status,
            'data' => ['HouseholdsServed' => $householdServed]
        ];

        return $response->withJson($data)->withStatus($data['status']);
    }
}