<?php
declare(strict_types=1);

namespace pantry\Reports;

use pantry\Slim\App;
use pantry\Slim\Authenticate;
use function valid_num_args;

class ServiceController
{
    public function register(App $slim)
    {
        assert(valid_num_args());

        $slim->get('/reports/service', ServiceReport::class)
            ->add(Authenticate::class);
    }
}