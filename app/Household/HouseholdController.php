<?php
declare(strict_types=1);

namespace pantry\Household;

use pantry\Slim\App;
use pantry\Slim\Authenticate;
use function valid_num_args;

class HouseholdController
{
    public function register(App $slim)
    {
        assert(valid_num_args());

        $slim->get('/households[/{id}]', HouseholdGetAction::class)
            ->add(Authenticate::class);
        $slim->post('/households', HouseholdPostAction::class)
            ->add(Authenticate::class);
        $slim->patch('/households', HouseholdPatchAction::class)
            ->add(Authenticate::class);

        $slim->get('/household-member-count/{id}', HouseholdMemberCountAction::class)
            ->add(Authenticate::class);
    }
}
