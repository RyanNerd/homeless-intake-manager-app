<?php
declare(strict_types=1);

namespace pantry\Household;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\Household;

class HouseholdPostAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        // Assume failure
        $data =
            [
                'success' => false,
                'status' => 500,
                'data' => null
            ];

        // Get the body as an associative array
        $body = $request->getParsedBody();

        // Add new Household record
        $houseHold = new Household();

        // Replace each key value from the JSON body into the Household model and save.
        foreach ($body as $key => $value) {
            if ($key !== 'Id') {
                $houseHold->$key = $value;
            }
        }

        if ($houseHold->save()) {
            $data = [
                'success' => true,
                'status' => 200,
                'data' => $houseHold
            ];
        }

        return $response->withJson($data)->withStatus($data['status']);
    }
}
