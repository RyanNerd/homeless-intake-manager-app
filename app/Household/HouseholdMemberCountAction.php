<?php
declare(strict_types=1);

namespace pantry\Household;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\Member;

class HouseholdMemberCountAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $route = $request->getAttribute('route');
        $id = $route->getArgument('id') ?? 0;

        $data = [
            'success' => false,
            'status' => 400,
            'data' => null
        ];

        if ($id > 0) {
            $memberCount = Member::where('HouseholdId', '=', $id)
                ->where('Active', '=', true)
                ->count();
            $data = [
                'success' => true,
                'status' => 200,
                'data' => ['Count' => $memberCount]
            ];
        }

        return $response->withJson($data)->withStatus($data['status']);
    }
}
