<?php
declare(strict_types=1);

namespace pantry\Validation;

class ValidatorSet
{
    /**
     * @var array<object>
     */
    protected $validators = [];

    /**
     * Add a validator to the set by key.
     */
    public function addValidator(string $key, callable $validator)
    {
        $this->validators[$key] = $validator;
    }

    /**
     * Return a validator by key.
     */
    public function getValidator(string $key)
    {
        return $this->validators[$key];
    }
}