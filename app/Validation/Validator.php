<?php
declare(strict_types=1);

namespace pantry\Validation;

use pantry\Slim\ResponseBody;
use Respect\Validation\Validator as V;
use function valid_num_args;

abstract class Validator
{
    protected const VALIDATOR_METHODS =
        [
            'intVal' => 'int',
            'alpha' => 'alpha',
            'date' => 'date'
        ];

    /**
     * Default allowed GET parameter keys
     */
    protected const DEFAULT_ALLOWED_GET_PARAMETERS =
    [
        'auth_key' => null,
        'order_by' => null,
        'order_direction' => ['asc', 'desc'],
        'soft_search' => ['0', '1', 'true', 'false']
    ];

    /**
     * Validate query parameters for GET requests and register any invalid keys.
     *
     * @param ResponseBody $responseBody
     * @param array $queryParameters
     * @param array $allowedQueryParameters
     */
    public function validateGETQueryParameters(ResponseBody &$responseBody, array $queryParameters, array $allowedQueryParameters = []): void
    {
        assert(valid_num_args());

        // Get the default allowed keys and merge them with the passed in allowed queryParameters
        $allowedParameterKeys = array_merge(array_keys(self::DEFAULT_ALLOWED_GET_PARAMETERS), array_keys($allowedQueryParameters));

        // Make sure we don't have any query parameter keys that are not allowed.
        foreach ($queryParameters as $key => $value) {
            // Is a query parameter key not known?
            if (!in_array($key, $allowedParameterKeys)) {
                $responseBody->registerParam('invalid', $key, 'unknown');
            }
        }

        // Validate query parameters against the default if they have enums
        foreach(self::DEFAULT_ALLOWED_GET_PARAMETERS as $key => $enums) {
            if (isset($queryParameters[$key]) && $enums !== null) {
                if (!in_array($queryParameters[$key], $enums)) {
                    $responseBody->registerParam('invalid', $key, 'enum');
                }
            }
        }

        // Validate the query parameters against the validation types.
        foreach($allowedQueryParameters as $key => $validationType) {
            // Do we have a query parameter and value that needs to be validated?
            if (isset($queryParameters[$key]) && $validationType !== null) {
                // Is the query parameter value not valid?
                if (V::not(V::$validationType())->validate($queryParameters[$key])) {
                    // Register the query parameter as invalid.
                    $responseBody->registerParam('invalid', $key, self::VALIDATOR_METHODS[$validationType]);
                }
            } else {
                $responseBody->registerParam('optional', $key, self::VALIDATOR_METHODS[$validationType]);
            }
        }
    }
}