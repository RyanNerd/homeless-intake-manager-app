<?php
declare(strict_types=1);

namespace pantry\Models;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $Id PK
 * @property int $UserId FK (not enforced) to User that last changed this record.
 * @property int $MemberId FK (not enforced) to Member that is head of household.
 * @property string $HouseholdName Household name (usually last name of head of the household)
 * @property string $Address
 * @property string $PO
 * @property string $City
 * @property string $State
 * @property string $Zip
 * @property string $County
 * @property string $Phone
 * @property float $Rent
 * @property bool $RentSubsidized
 * @property bool $Own
 * @property bool $WithFriendsFamily
 * @property bool $Homeless
 * @property bool $FoodStamps
 * @property bool $WIC
 * @property bool $FreeSchoolLunch
 * @property bool $Medicaid
 * @property string $VehicleMake
 * @property string $VehicleModel
 * @property string $VehicleYear
 * @property int $FamilyType 1=Single,2=Single Parent/Female,3=Single Parent/Male,4=Married (w/children),5=Married (no children),6=Single w/Partner,7=Multiple Adults (living w/children),8=Multiple Adluts (no children),9=Grandparent (raising Grandchildren),10=Other
 * @property bool $Active
 * @property bool $IsDemo - True if this household is to be excluded from reports and stats
 * @property string $Created Date record was created
 * @property string $Changed Date record was last changed

 * @mixin Builder
 */
class Household extends ModelBase
{
    public $table = 'Household';

    function save(array $options = [])
    {
        $this->RentSubsidized = $this->RentSubsidized ?? false;
        $this->Own = $this->Own ?? false;
        $this->WithFriendsFamily = $this->WithFriendsFamily ?? false;
        $this->Homeless = $this->Homeless ?? false;
        $this->FoodStamps = $this->FoodStamps ?? false;
        $this->WIC = $this->WIC ?? false;
        $this->FreeSchoolLunch = $this->FreeSchoolLunch ?? false;
        $this->Medicaid = $this->Medicaid ?? false;
        $this->Active = $this->Active ?? true;
        $this->IsDemo = $this->IsDemo ?? false;

        return parent::save($options);
    }
}
