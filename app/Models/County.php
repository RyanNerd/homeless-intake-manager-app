<?php
declare(strict_types=1);

namespace pantry\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * @property string $Id PK
 * @property string $StateCode
 * @property string $CountyCode
 * @property string $CountyName
 *
 * @mixin Builder
 */
class County extends ModelBase
{
    public $table = 'County';
}
