<?php
declare(strict_types=1);

namespace pantry\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $Id PK
 * @property int $HouseholdId FK to Household
 * @property int $MemberId FK to Member
 * @property int $SignatureId FK to Storage
 * @property int $UserId FK to User that last changed this record.
 * @property bool $FoodBox
 * @property bool $Perishable
 * @property bool $Camper
 * @property bool $Diaper
 * @property string $Notes Intake notes
 * @property float $FoodBoxWeight # of LBS at weigh out
 * @property float $PerishableWeight # of LBS at weigh out
 * @property int $HouseholdSize Current # of members of the household
 * @property int $IntakeYear
 * @property int $IntakeMonth
 * @property int $IntakeDay
 * @property string $Created Date record was created
 * @property string $Changed Date record was last changed
 *
 * @mixin Builder
 */
class Intake extends ModelBase
{
    public $table = 'Intake';
}
