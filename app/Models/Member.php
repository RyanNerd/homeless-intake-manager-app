<?php
declare(strict_types=1);

namespace pantry\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $Id PK
 * @property int $HouseholdId FK to Household
 * @property int $PhotoId FK Storage
 * @property int $UserId FK to User that last changed this record
 * @property string $LastName Member last name
 * @property string $FirstName Member first name
 * @property string $MiddleInitial
 * @property string $DOB
 * @property int $BirthYear
 * @property int $BirthMonth
 * @property int $BirthDay
 * @property string $Email
 * @property string $Phone
 * @property string $Gender
 * @property bool $Disability
 * @property bool $Veteran
 * @property string $Race A=Asian, B=Black, H=Hispanic, N=Native American, P=Pacific Islander, W=White, O=Other
 * @property bool $Hispanic
 * @property bool $Education I=Infant(0-3 yrs),P=Preschool,K=Kindergarten,Y=Youth (1st-6th grade),9=9th or less,10=10th,11=11th,12=12th
 * @property bool $EducationAssociate
 * @property bool $EducationBachelors
 * @property bool $CanWork
 * @property bool $Employed
 * @property string $IncomeType W=Weekly, B=Bi-Monthly, M=Monthly, A=Annual
 * @property float $IncomeTotal
 * @property bool $IncomeSocialSecurity
 * @property bool $IncomeSSI
 * @property bool $IncomeChildSupport
 * @property string $IncomeOther
 * @property bool $HealthInsurance
 * @property bool $HealthInsurancePrivate
 * @property bool $HealthInsuranceMedicaid
 * @property bool $HealthInsuranceMedicare
 * @property bool $HealthInsuranceCHIP
 * @property bool $HealthInsurancePCN
 * @property bool $Active
 * @property string $Created Date record was created
 * @property string $Changed Date record was last changed
 * @mixin Builder
 */
class Member extends ModelBase
{
    public $table = 'Member';
}
