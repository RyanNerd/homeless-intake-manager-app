<?php
declare(strict_types=1);

namespace pantry\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $Id PK - Don't usually have the PK have meaning but it this case it is household size
 * @property int $Monthly - Monthly income cap as a poverty guideline
 *
 * @mixin Builder
 */
class Poverty extends ModelBase
{
    public $table = 'Poverty';
}
