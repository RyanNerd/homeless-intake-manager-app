<?php
declare(strict_types=1);

namespace pantry\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $Id PK
 * @property string $UserName Unique Id
 * @property string $LastName User last name
 * @property string $FirstName User first name
 * @property string $Email User's email
 * @property string $PasswordHash User's password as a hash value
 * @property bool $MustResetPassword Flag indicating if the password must be reset on next login
 * @property bool   $IsAdmin User is or is not an admin
 * @property string $AuthKey a 32 byte string used to authenticate API requests
 * @property bool $Active
 *
 * @mixin Builder
 */
class User extends ModelBase
{
    public $table = 'User';
}