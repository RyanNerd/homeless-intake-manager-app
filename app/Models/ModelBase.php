<?php
declare(strict_types=1);

namespace pantry\Models;

use Illuminate\Database\Eloquent\Model;

class ModelBase extends Model
{
    public $timestamps = false;
    public $primaryKey = "Id";
}
