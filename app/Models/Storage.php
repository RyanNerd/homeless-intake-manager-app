<?php
declare(strict_types=1);

namespace pantry\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $Id PK
 * @property string $FileName File name for the image
 * @property string $MimeType Image mime type (image/jpeg, image/png, or image/gif)
 *
 * @mixin Builder
 */
class Storage extends ModelBase
{
    public $table = 'Storage';
}