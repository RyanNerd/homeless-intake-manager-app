<?php
declare(strict_types=1);

namespace pantry\Member;

use pantry\Slim\ResponseBody;
use function valid_num_args;
use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use pantry\Models\Member;
use pantry\Models\Household;

/**
 * Class MemberPostAction
 * Required:
 *  - HouseholdId
 *  - DOB
 */
class MemberPostAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        // Assume failure
        $data =
            [
                'success' => false,
                'status' => 500,
                'data' => null
            ];

        $attributes = $request->getAttributes();

        /**
         * @var array
         */
        $body = $attributes['request_body'];

        /**
         * @var ResponseBody $responseBody
         */
        $responseBody = $attributes['response_body'];

        $householdData = [];
        $householdId = $body['HouseholdId'] ?? 0;

        // TODO: Move validation logic to MemberWriteValidator.
        // TODO: If householdId is 0 then we should bail with error.

        // Add new Member record
        $member = new Member();

        // Replace each key value from the JSON body into the Member model and save.
        foreach ($body as $key => $value) {

            // Special handling for DOB
            if ($key === 'DOB') {
                if ($value !== null) {
                    $value = $value . ' 00:00:00';
                }
            }

            $member->$key = $value;
        }

        if ($member->save()) {
            $data = [
                'success' => true,
                'status' => 200,
                'data' => array_merge($member->getAttributes(), $householdData)
            ];
        } else {
            //TODO: What went wrong?
            // TODO: Look at how MemberPatchAction handles this crap
        }

        return $response->withJson($data)->withStatus($data['status']);
    }
}
