<?php
declare(strict_types=1);

namespace pantry\Member;

use pantry\Slim\App;
use pantry\Slim\Authenticate;
use function valid_num_args;

class MemberController
{
    protected $validatorSet;

    public function register(App $slim)
    {
        assert(valid_num_args());

        $slim->get('/members[/{id}]', MemberGetAction::class)
            ->add(MemberGetValidator::class)
            ->add(Authenticate::class);
        $slim->post('/members', MemberPostAction::class)
            ->add(MemberWriteValidator::class)
            ->add(Authenticate::class);
        $slim->patch('/members', MemberPatchAction::class)
            ->add(MemberWriteValidator::class)
            ->add(Authenticate::class);
    }
}



