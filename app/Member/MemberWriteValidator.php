<?php
declare(strict_types=1);

namespace pantry\Member;

use pantry\Slim\ResponseBody;
use pantry\Validation\Validator;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use function valid_num_args;

use Respect\Validation\Validator as V;

class MemberWriteValidator extends Validator
{
    /**
     * PATCH/POST requests are validated via Slim's middleware.
     *
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, callable $next): ResponseInterface
    {
        assert(valid_num_args());

        // $httpMethod = $request->getMethod();
        $body = $request->getParsedBody();

        /**
         * @var ResponseBody $responseBody
         */
        $responseBody = $request->getAttribute('response_body');

        // TODO: Actual validations

        //
        // REQUIRED
        //
        //if (!V::exists()->validate($body['HouseholdId'])) {
        //    $responseBody->registerParam('required', 'HouseholdId', 'int');
        //}

        if ($responseBody->hasMissingRequiredOrInvalid()) {
            // No further processing. Return a response indicating an invalid request.
            return $response->withJson($responseBody->withStatus(400)())->withStatus(400);
        }

        return $next($request->withAttribute('request_body', $body), $response);
    }
}
