<?php
declare(strict_types=1);

namespace pantry\Member;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\Member;
use Illuminate\Support\Facades\DB;

class MemberSearchAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $results = null;

        $searchParams = $request->getQueryParams();
        $lastName = $searchParams['last_name'] ?? '';
        $firstName = $searchParams['first_name'] ?? '';
        $birthYear = $searchParams['birth_year'] ?? '';
        $dob = $searchParams['dob'] ?? '';
        $householdId = $searchParams['household_id'] ?? '';

        if (strlen($householdId) > 0) {
            $results = Member::where('HouseholdId', '=', $householdId)->get();
        } else {
            if (strlen($lastName) > 0 && strlen($firstName) > 0) {
                $member = Member::where([
                    ['LastName', 'LIKE', $lastName . '%'],
                    ['FirstName', 'LIKE', $firstName . '%']
                ]);
            } else {
                if (strlen($lastName) > 0) {
                    $member = Member::where('LastName', 'LIKE', $lastName . '%');
                }

                if (strlen($firstName) > 0) {
                    $member = Member::where('FirstName', 'LIKE', $firstName . '%');
                }
            }

            if (isset($member)) {
                if (!empty($birthYear)) {
                    $member->whereYear('DOB', $birthYear);
                }

                if (!empty($dob)) {
                    $member->whereDate('DOB', $dob);
                }

                $results = $member->get();
            }
        }

        $status = ($results === null) ? 404 : 200;
        $data = [
            'success' => ($status === 200),
            'status' => $status,
            'data' => $results
        ];

        return $response->withJson($data)->withStatus($data['status']);
    }
}
