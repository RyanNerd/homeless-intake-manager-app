<?php
declare(strict_types=1);

namespace pantry\Member;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use pantry\Slim\ResponseBody;
use function valid_num_args;
use pantry\Models\Member;

class MemberPatchAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $attributes = $request->getAttributes();

        /**
         * @var array
         */
        $body = $attributes['request_body'];

        /**
         * @var ResponseBody $responseBody
         */
        $responseBody = $attributes['response_body'];

        // Get the id we are patching
        $id = $body['Id'] ?? 0;

        // Only attempt the patch if we have a valid PK
        if ($id > 0) {

            // Look up the Member via the id (PK).
            $member = Member::find($id);

            // If member is NOT Null then we found an existing record.
            if ($member !== null) {
                foreach ($body as $key => $value) {
                    if (in_array($key, ['HouseHoldId', 'household_size', 'photo'])) {
                        continue;
                    }
                    $member->$key = $value;
                }

                $member->UserId = $responseBody->getUserId();

                // The save() method will return true if we updated the record.
                if ($member->save()) {
                    $responseBody = $responseBody->withData($member->toArray())->withStatus(200);
                } else {
                    // Status 409 is conflict (why would save fail otherwise?)
                    $responseBody = $responseBody->withStatus(409)->withMessage('Unable to update Member');
                }
            } else {
                $responseBody = $responseBody->withStatus(404)->withMessage('Member not found');
            }
        } else {
            $responseBody->registerParam('required', 'id', 'int');
            $responseBody = $responseBody->withStatus(400);
        }

        return $response->withJson($responseBody())->withStatus($responseBody->getStatus());
    }
}
