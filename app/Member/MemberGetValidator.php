<?php
declare(strict_types=1);

namespace pantry\Member;

use pantry\Slim\ResponseBody;
use pantry\Validation\Validator;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use function valid_num_args;

class MemberGetValidator extends Validator
{
    protected const ALLOWED_QUERY_PARAMETERS =
    [
        '_household_id' => 'intVal',
        '_last_name' => 'alpha',
        '_first_name' => 'alpha',
        '_dob' => 'date'
    ];

    /**
     * GET requests are validated via Slim's middleware.
     *
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return ResponseInterface
     */
    public function __invoke(Request $request, Response $response, callable $next): ResponseInterface
    {
        assert(valid_num_args());

        // Get query parameters from the GET request
        $queryParameters = $request->getQueryParams();

        // Only validate query parameters if there any to validate.
        if (count($queryParameters) !== 0) {
            /** @var ResponseBody $responseBody */
            $responseBody = $request->getAttribute('response_body');
            $this->validateGETQueryParameters($responseBody, $queryParameters, self::ALLOWED_QUERY_PARAMETERS);

            // Any invalid or required missing?
            if ($responseBody->hasMissingRequiredOrInvalid()) {
                // No further processing. Return a response indicating an invalid request.
                return $response->withJson($responseBody->withStatus(400)())->withStatus(400);
            }
        }

        // All validations passed so continue processing middleware.
        return $next($request, $response);
    }
}
