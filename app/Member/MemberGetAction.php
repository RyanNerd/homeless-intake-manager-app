<?php
declare(strict_types=1);

namespace pantry\Member;

use pantry\Slim\ActionBase;
use pantry\Slim\ResponseBody;
use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use pantry\Models\Member;
use Slim\Route;
use function valid_num_args;

/**
 * Class MemberGetAction
 * SEDI - Slim, Eloquent, Dependency Injection
 */
class MemberGetAction extends ActionBase
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $attributes = $request->getAttributes();

        /** @var ResponseBody $responseBody */
        $responseBody = $attributes['response_body'];

        /** @var Route $route */
        $route = $attributes['route'];
        $id = $route->getArgument('id');
        if (isset($id) && strlen($id) !== 0) {
            $id = (int)$id;
            if ($id > 0) {
                $members = Member::find($id);
                $members = isset($members) ? [$members->toArray()] : null;
            } else {
                $responseBody = $responseBody->
                    withStatus(400)->
                    withMessage('invalid Id');
                return $response->withJson($responseBody())->withStatus(400);
            }
        } else {
            $members = $this->buildSelect(new Member(), $request->getQueryParams());

            if (count($members) === 0) {
                $members = null;
            } else {
                $members = $members->toArray();
            }
        }

        $responseBody = $responseBody->withData($members)->withStatus(($members === null) ? 404 : 200);
        return $response->withJson($responseBody())->withStatus($responseBody->getStatus());
    }
}
