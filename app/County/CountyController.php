<?php
declare(strict_types=1);

namespace pantry\County;

use pantry\Slim\App;
use function valid_num_args;

class CountyController
{
    public function register(App $slim)
    {
        assert(valid_num_args());

        $slim->get('/counties/{state_code}', CountyGetAction::class);
    }
}