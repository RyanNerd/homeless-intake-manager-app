<?php
declare(strict_types=1);

namespace pantry\County;

use pantry\Models\County;
use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Http\Message\ResponseInterface;
use function valid_num_args;
use pantry\Models\Poverty;

class CountyGetAction
{
    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        assert(valid_num_args());

        $route = $request->getAttribute('route');
        $stateCode = $route->getArgument('state_code');
        $records = County::where('StateCode', '=', $stateCode)->get()->toArray();
        $status = (count($records) > 0) ? 200 : 404;

        $data = [
            'success' => ($status === 200),
            'status' => $status,
            'data' => $records
        ];

        return $response->withJson($data)->withStatus($data['status']);
    }
}
