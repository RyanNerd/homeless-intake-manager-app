<?php
declare(strict_types=1);

use function DI\get;
use function DI\object;
use pantry\Slim\HandleRuntimeErrors;

$showErrors = (strtoupper(getenv('DEVELOPMENT') ?? 'FALSE') === 'TRUE');

return [
    'settings.displayErrorDetails' => $showErrors,
    'settings.determineRouteBeforeAppMiddleware' => true,

    'errorHandler' => object(HandleRuntimeErrors::class)
        ->constructor(get('settings.displayErrorDetails'))
];
