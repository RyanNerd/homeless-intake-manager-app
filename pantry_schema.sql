CREATE DATABASE IF NOT EXISTS `pantry` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pantry`;
-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: pantry
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `County`
--

DROP TABLE IF EXISTS `County`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `County` (
  `Id`         int(11)     NOT NULL AUTO_INCREMENT,
  `StateCode`  varchar(2)  NOT NULL,
  `CountyCode` varchar(5)  NOT NULL COMMENT 'See:https://www.census.gov/geo/reference/codes/cou.html\nCounty Code and PK',
  `CountyName` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `county_state_code_idx` (`StateCode`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3236
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Household`
--

DROP TABLE IF EXISTS `Household`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Household` (
  `Id`                int(11)    NOT NULL AUTO_INCREMENT,
  `UserId`            int(11)             DEFAULT NULL COMMENT 'FK to the last User to modify this record.Informational only.',
  `MemberId`          int(11)             DEFAULT NULL COMMENT 'FK to the Member that is the head of household.',
  `HouseholdName`     varchar(45)         DEFAULT NULL,
  `Address`           varchar(45)         DEFAULT NULL,
  `PO`                varchar(45)         DEFAULT NULL,
  `City`              varchar(45)         DEFAULT NULL,
  `State`             char(2)             DEFAULT 'UT',
  `Zip`               varchar(10)         DEFAULT NULL,
  `County`            varchar(5)          DEFAULT NULL,
  `Phone`             varchar(20)         DEFAULT NULL,
  `Rent`              float(7, 2)         DEFAULT NULL,
  `RentSubsidized`    tinyint(4) NOT NULL DEFAULT '0',
  `Own`               tinyint(4) NOT NULL DEFAULT '0',
  `WithFriendsFamily` tinyint(4) NOT NULL DEFAULT '0',
  `Homeless`          tinyint(4) NOT NULL DEFAULT '0',
  `FoodStamps`        tinyint(4) NOT NULL DEFAULT '0',
  `WIC`               tinyint(4) NOT NULL DEFAULT '0',
  `FreeSchoolLunch`   tinyint(4) NOT NULL DEFAULT '0',
  `Medicaid`          tinyint(4) NOT NULL DEFAULT '0',
  `VehicleMake`       varchar(45)         DEFAULT NULL,
  `VehicleModel`      varchar(45)         DEFAULT NULL,
  `VehicleYear`       varchar(45)         DEFAULT NULL,
  `FamilyType`        tinyint(4)          DEFAULT NULL COMMENT ' 1=Single\n2=Single Parent/Female\n3=SIngle Parent/Male\n4=Married (w/children)\n5=Married (no children)\n6=Single w/Partner\n7=Multiple Adults (living w/children)\n8=Multiple Adluts (no children)\n9=Grandparent (raising Grandchildren)\n10 = Other',
  `Active`            tinyint(4) NOT NULL DEFAULT '1',
  `IsDemo`            tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Indicates if the Household is Demo (Any households marked as such will be excluded from reporting and statistics).',
  `Changed`           datetime   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Created`           datetime   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Intake`
--

DROP TABLE IF EXISTS `Intake`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Intake` (
  `Id`               int(11)    NOT NULL AUTO_INCREMENT,
  `HouseholdId`      int(11)    NOT NULL,
  `MemberId`         int(11)    NOT NULL,
  `SignatureId`      int(11)             DEFAULT NULL,
  `UserId`           int(11)             DEFAULT NULL,
  `FoodBox`          tinyint(4) NOT NULL DEFAULT '0',
  `Perishable`       tinyint(4) NOT NULL DEFAULT '0',
  `Camper`           tinyint(4) NOT NULL DEFAULT '0',
  `Diaper`           tinyint(4) NOT NULL DEFAULT '0',
  `Notes`            text,
  `FoodBoxWeight`    float(10, 2)        DEFAULT '0.00',
  `PerishableWeight` float(10, 2)        DEFAULT '0.00',
  `HouseholdSize`    tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Household size at the time of intake.',
  `IntakeYear`       int(11)             DEFAULT NULL,
  `IntakeMonth`      tinyint(4)          DEFAULT NULL,
  `IntakeDay`        tinyint(4)          DEFAULT NULL,
  `Created`          datetime   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Changed`          datetime   NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `fk_Intake_household_idx` (`HouseholdId`),
  KEY `fk_Intake_member_idx` (`MemberId`),
  CONSTRAINT `fk_Intake_Household` FOREIGN KEY (`HouseholdId`) REFERENCES `Household` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Intake_Member` FOREIGN KEY (`MemberId`) REFERENCES `Member` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 15
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Member`
--

DROP TABLE IF EXISTS `Member`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Member` (
  `Id`                      int(11)       NOT NULL           AUTO_INCREMENT,
  `HouseholdId`             int(11)       NOT NULL,
  `PhotoId`                 int(11)                          DEFAULT NULL,
  `UserId`                  int(11)                          DEFAULT NULL
  COMMENT ' FK to the last User to modify this record.Informational only.',
  `LastName`                varchar(50)                      DEFAULT NULL,
  `FirstName`               varchar(50)                      DEFAULT NULL,
  `MiddleInitial`           char(1)                          DEFAULT NULL,
  `DOB`                     datetime                         DEFAULT NULL,
  `BirthYear`               int(11)                          DEFAULT NULL,
  `BirthMonth`              tinyint(4)                       DEFAULT NULL,
  `BirthDay`                tinyint(4)                       DEFAULT NULL,
  `Email`                   varchar(100)                     DEFAULT NULL,
  `Phone`                   varchar(10)                      DEFAULT NULL,
  `Gender`                  char(1)                          DEFAULT NULL,
  `Disability`              tinyint(4)    NOT NULL           DEFAULT '0',
  `Veteran`                 tinyint(4)    NOT NULL           DEFAULT '0',
  `Race`                    varchar(1)    NOT NULL           DEFAULT 'W' COMMENT 'A = Asian,
  B = Black,
  H = Hispanic,
  N = Native American,
  P = Pacific Islander,
  W = White,
  O = Other',
  `Hispanic`                tinyint(4)    NOT NULL           DEFAULT '0',
  `Education`               varchar(2) CHARACTER SET big5    DEFAULT NULL
  COMMENT ' I=Infant (0-3 yrs)\nP=Preschool\nK=Kindergarten\nY=Youth (1st-6th grade)\n9=9th or less \n10=10th\n11 = 11th\n12 = 12th',
  `EducationAssociate`      tinyint(4)    NOT NULL           DEFAULT '0',
  `EducationBachelors`      tinyint(4)    NOT NULL           DEFAULT '0',
  `CanWork`                 tinyint(4)    NOT NULL           DEFAULT '0',
  `Employed`                tinyint(4)    NOT NULL           DEFAULT '0',
  `IncomeType`              char(1)                          DEFAULT NULL
  COMMENT ' W=Weekly\nB=Bi-Monthly\nM = Monthly\nA = Annual',
  `IncomeTotal`             decimal(7, 2) NOT NULL,
  `IncomeSocialSecurity`    tinyint(4)    NOT NULL           DEFAULT '0',
  `IncomeSSI`               tinyint(4)    NOT NULL           DEFAULT '0',
  `IncomeChildSupport`      tinyint(4)    NOT NULL           DEFAULT '0',
  `IncomeOther`             varchar(45)                      DEFAULT NULL,
  `HealthInsurance`         tinyint(4)    NOT NULL           DEFAULT '0',
  `HealthInsurancePrivate`  tinyint(4)    NOT NULL           DEFAULT '0',
  `HealthInsuranceMedicaid` tinyint(4)    NOT NULL           DEFAULT '0',
  `HealthInsuranceMedicare` tinyint(4)    NOT NULL           DEFAULT '0',
  `HealthInsuranceCHIP`     tinyint(4)    NOT NULL           DEFAULT '0',
  `HealthInsurancePCN`      tinyint(4)    NOT NULL           DEFAULT '0',
  `Active`                  tinyint(4)    NOT NULL           DEFAULT '1',
  `Changed`                 datetime      NOT NULL           DEFAULT CURRENT_TIMESTAMP
  ON UPDATE CURRENT_TIMESTAMP,
  `Created`                 datetime      NOT NULL           DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `fk_Member_Household_idx` (`HouseholdId`),
  CONSTRAINT `fk_Member_Household` FOREIGN KEY (`HouseholdId`) REFERENCES `Household` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 19
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Poverty`
--

DROP TABLE IF EXISTS `Poverty`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Poverty` (
  `Id`      int(11)  NOT NULL,
  `UserId`  int(11)           DEFAULT NULL
  COMMENT ' FK to the last User to modify this record.Informational only.',
  `Monthly` int(11)  NOT NULL,
  `Changed` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
  ON UPDATE CURRENT_TIMESTAMP,
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Storage`
--

DROP TABLE IF EXISTS `Storage`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Storage` (
  `Id`       int(11) NOT NULL AUTO_INCREMENT,
  `FileName` varchar(30)      DEFAULT NULL,
  `MimeType` varchar(45)      DEFAULT NULL,
  `Changed`  datetime         DEFAULT CURRENT_TIMESTAMP
  ON UPDATE CURRENT_TIMESTAMP,
  `Created`  datetime         DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 30
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `Id`                int(11)      NOT NULL AUTO_INCREMENT,
  `UserName`          varchar(45)           DEFAULT NULL,
  `LastName`          varchar(45)           DEFAULT NULL,
  `FirstName`         varchar(45)           DEFAULT NULL,
  `Email`             varchar(150)          DEFAULT NULL,
  `PasswordHash`      varchar(100) NOT NULL,
  `MustResetPassword` tinyint(4)   NOT NULL,
  `IsAdmin`           tinyint(4)   NOT NULL DEFAULT '0',
  `AuthKey`           char(36)     NOT NULL,
  `Active`            tinyint(4)   NOT NULL DEFAULT '1',
  `Created`           datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Changed`           datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP
  ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Email_UNIQUE` (`Email`),
  UNIQUE KEY `user_user_name_idx` (`UserName`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = latin1;

ALTER TABLE Member AUTO_INCREMENT=1001;

/*!40101 SET character_set_client = @saved_cs_client */;

/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;