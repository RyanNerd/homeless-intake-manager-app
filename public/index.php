<?php
declare(strict_types=1);

// If we are getting a pre-flight CORS request then handle it without going through Slim
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    ob_start();
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, OPTIONS');

    header(sprintf(
        'HTTP/%s %s %s',
        "1.1",
        200,
        'OK'
    ));
    ob_end_flush();

    exit();
}

require_once __DIR__ . '/../vendor/autoload.php';

use pantry\Slim\App;

$requestUri = $_SERVER['REQUEST_URI'];
if ($requestUri === '/index.php/pantry' || $requestUri === '/pantry')
{
    $newUri = $_SERVER['HTTP_HOST'] . '/pantry.html';
    ob_start();
    header('Location: '. $newUri);
    ob_end_flush();
    exit();
}

$app = new App();
$app->run();
